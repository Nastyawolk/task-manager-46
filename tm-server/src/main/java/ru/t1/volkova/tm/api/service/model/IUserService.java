package ru.t1.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.model.IUserRepository;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.User;

import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface IUserService {

    @NotNull IUserRepository getRepository();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    );

    @Nullable List<User> findAll();

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    User removeOne(@Nullable User model);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}
