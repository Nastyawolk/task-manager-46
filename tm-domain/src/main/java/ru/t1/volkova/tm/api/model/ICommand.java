package ru.t1.volkova.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    void execute();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

}
